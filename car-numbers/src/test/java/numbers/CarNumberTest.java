package numbers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import numbers.data.CarNumberRepository;

@SpringBootTest
public class CarNumberTest {

	@Autowired
	private CarNumberRepository repo;

	@Autowired
	private CarNumberService service;

	@Test
	public void testGetFirstCarNumber() throws Exception {
		var number = service.getNextNumber();
		assertEquals("А000АА 116 RUS", number.getFormattedNumber());
	}

	@Test
	public void testGetNextCarNumber() throws Exception {
		var number = CarNumber.builder().digits(999).letters(new char[] { 'Т', 'А', 'Х' }).region("116 RUS")
				.formattedNumber("Т999АХ 116 RUS").build();
		repo.save(number);
		var nextNumber = service.getNextNumber();
		assertEquals("Т000ВХ 116 RUS", nextNumber.getFormattedNumber());
	}

	@AfterEach
	private void clear() {
		repo.deleteAll();
	}

}
