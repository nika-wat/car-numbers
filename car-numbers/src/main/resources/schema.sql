create table if not exists Car_Number (
id identity,
letters varchar(3) not null,
digits bigint not null,
region text not null,
create_date timestamp not null,
formatted_number text not null
);