package numbers;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum AllowedLetters {

	А('А', 1), В('В', 2), Е('Е', 3), К('К', 4), М('М', 5), Н('Н', 6), О('О', 7), Р('Р', 8), С('С', 9), Т('Т', 10),
	У('У', 11), Х('Х', 12);

	private static Map<Character, Integer> map = new HashMap<>();

	private final char letter;

	private final int order;

	static {
		map.put('А', 1);
		map.put('В', 2);
		map.put('Е', 3);
		map.put('К', 4);
		map.put('М', 5);
		map.put('Н', 6);
		map.put('О', 7);
		map.put('Р', 8);
		map.put('С', 9);
		map.put('Т', 10);
		map.put('У', 11);
		map.put('Х', 12);
	}

	public static char getCharByOrder(int order) {
		return AllowedLetters.values()[order - 1].letter;
	}

	public static int getOrderByChar(char letter) {
		return map.get(letter);
	}

}
