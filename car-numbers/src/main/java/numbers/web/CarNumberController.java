package numbers.web;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import numbers.CarNumber;
import numbers.CarNumberService;

@RestController
@RequestMapping("/number")
public class CarNumberController {

	private CarNumberService service;

	public CarNumberController(CarNumberService service) {
		this.service = service;
	}

	@ResponseStatus(HttpStatus.CREATED)
	@GetMapping(value = "/random", produces = "text/plain; charset=utf-8")
	public String getRandomNumber() {
		CarNumber number = service.getRandomNumber();
		return number.getFormattedNumber();
	}

	@ResponseStatus(HttpStatus.CREATED)
	@GetMapping(value = "/next", produces = "text/plain; charset=utf-8")
	public String getNextNumber() {
		CarNumber number = service.getNextNumber();
		return number.getFormattedNumber();
	}

}
