package numbers;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.validation.constraints.Size;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.persistence.Id;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class CarNumber implements Serializable {

	private static final long serialVersionUID = 4304302596468606381L;

	@Id
	@GeneratedValue
	private final long id;

	@NotNull
	@Size(min = 3, max = 3, message = "Только 3 буквы")
	private final char[] letters;

	@NotNull
	@Min(0)
	@Max(999)
	private final int digits;

	@NotNull
	private final String region;

	@NotNull
	private final LocalDateTime createDate = LocalDateTime.now();

	@NotNull
	private final String formattedNumber;

}
