package numbers.data;

import org.springframework.data.jpa.repository.JpaRepository;

import numbers.CarNumber;

public interface CarNumberRepository extends JpaRepository<CarNumber, Long> {
	
	CarNumber findFirstByOrderByCreateDateDesc();

}
