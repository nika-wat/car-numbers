package numbers;

import java.util.Random;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import numbers.data.CarNumberRepository;

@Service
public class CarNumberService {

	private CarNumberRepository repo;

	public CarNumberService(CarNumberRepository repo) {
		this.repo = repo;
	}

	private static final String REGION = "116 RUS";

	public CarNumber getNextNumber() {
		var lastCarNumber = repo.findFirstByOrderByCreateDateDesc();
		if (null == lastCarNumber) {
			return createFirstCarNumber();
		}
		CarNumber number = null;
		int digits = lastCarNumber.getDigits();
		var letters = lastCarNumber.getLetters();
		if (digits < 999) {
			digits++;
			number = createCarNumber(letters, digits);
		} else {
			letters = getNextLetters(letters);
			number = createCarNumber(letters, 0);
		}
		repo.save(number);
		return number;
	}

	private CarNumber createFirstCarNumber() {
		char firstLetter = AllowedLetters.getCharByOrder(1);
		CarNumber number = createCarNumber(new char[] { firstLetter, firstLetter, firstLetter }, 0);
		repo.save(number);
		return number;
	}

	private CarNumber createCarNumber(char[] letters, int digits) {
		return CarNumber.builder().letters(letters).digits(digits).region(REGION)
				.formattedNumber(getFormattedNumber(letters, digits)).build();
	}

	private String getFormattedNumber(char[] letters, int digits) {
		return new StringBuilder().append(letters[0]).append(String.format("%03d", digits)).append(letters[1])
				.append(letters[2]).append(" ").append(REGION).toString();
	}

	private char[] getNextLetters(char[] letters) {
		int currentOrder;
		int maxOrder = AllowedLetters.values().length;
		for (int i = letters.length - 1; i >= 0; i--) {
			currentOrder = AllowedLetters.getOrderByChar(letters[i]);
			if (currentOrder < maxOrder) {
				letters[i] = AllowedLetters.getCharByOrder(currentOrder + 1);
				break;
			}
			if (0 == i) {
				throw new IllegalStateException("Last available car number.");
			}
		}
		return letters;
	}

	public CarNumber getRandomNumber() {
		var all = repo.findAll().stream().map(CarNumber::getFormattedNumber).collect(Collectors.toSet());
		CarNumber number;
		do {
			number = generateRandomNumber();
		} while (all.contains(number.getFormattedNumber()));
		repo.save(number);
		return number;
	}

	private CarNumber generateRandomNumber() {
		Random rnd = new Random();
		int digits = rnd.nextInt(1000);
		AllowedLetters[] values = AllowedLetters.values();
		int lettersAmount = values.length;
		char firstLetter = values[rnd.nextInt(lettersAmount)].getLetter();
		char secondLetter = values[rnd.nextInt(lettersAmount)].getLetter();
		char thirdLetter = values[rnd.nextInt(lettersAmount)].getLetter();
		var letters = new char[] { firstLetter, secondLetter, thirdLetter };
		return createCarNumber(letters, digits);
	}

}
